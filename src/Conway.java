/**
 * Conway game
 * <p>
 * Cells - dead one gets alive if there's 2 or 3 alive around
 * dies when...
 * Get it later from real interview. RN only need to traverse
 */
public class Conway {

    public static int iterate(int[][] current, int r, int c /*, consumer to do all kind of things */) {
        // for each cell, count alive neighbours and
        // set a new state
        if (null == current || 0 == current[0].length) {
            return 0;
        }
        int count = 0;
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (x != 0 || y != 0) {
                    if (r + x >= 0 && r + x < current.length && c + y >= 0 && c + y < current[0].length) {
                        System.out.printf("r: %d, c: %d, cell: %d%n", r + x, c + y, current[r + x][c + y]);
                        count++;
                    }
                }
            }
        }
        System.out.println("count: " + count);
        return count;
    }

    public static void main(String[] args) {
        int F1[][] =
                new int[][]{
                        {1},
                };

        int F2[][] =
                new int[][]{
                        {1, 1},
                        {0, 1},
                };

        int F3[][] =
                new int[][]{
                        {1, 1, 0},
                        {0, 1, 0},
                        {1, 0, 0},
                };

        int F4[][] =
                new int[][]{
                        {1, 1, 0, 0},
                        {0, 1, 0, 0},
                        {1, 0, 0, 1},
                        {0, 0, 0, 0},
                };

        int F5[][] =
                new int[][]{
                        {1, 1, 0, 0, 0},
                        {0, 1, 0, 0, 1},
                        {1, 0, 0, 1, 1},
                        {0, 0, 0, 0, 0},
                        {1, 0, 1, 0, 1}
                };

        int[][] current = F4;

        int count = 0;
        for (int row = 0; row < current.length; row++) {
            for (int col = 0; col < current[0].length; col++) {
                count += iterate(current, row, col);
            }
        }
        System.out.println("total count: " + count);
    }


}
